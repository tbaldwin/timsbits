#!/bin/bash

set -x
set -o pipefail
unset "${!GIT_@}"

(
git cherry-pick -n v5.0.0..RO_Fixes_Stable || (
    git cherry-pick --abort 
    git reset --hard
    git cherry-pick -n origin/master..master
) || exit 125

make -C ../qemu-build -j4 || exit 125

{
    printf 'BASIC\nSYS "IXSupport_LinuxSyscall",,,,,,,,248\n' | timeout 10 bwrap --unshare-all --proc /proc --ro-bind /usr /usr --ro-bind /lib64 /lib64 --symlink /usr/lib /lib --ro-bind ~/RISC_OS/src/Unix/RISC_OS/RISC_OS /r --ro-bind ../qemu-build/arm-linux-user/qemu-arm /qemu --dev /dev /qemu /r  || exit 1
} |& tee log
)
r=$?

git cherry-pick --abort
git reset --hard

grep 'Unable to allocate 0x100000000 bytes of virtual address space' log && exit 125

exit $r
