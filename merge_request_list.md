# Split dependencies
* RiscOS/Env!4
* RiscOS/BuildSys!8
* RiscOS/Sources/Lib/Email/EmailCommon!1
* RiscOS/Sources/Networking/Omni/Apps/Omni!3
* RiscOS/Tools/Sources/TarExtend!1
* RiscOS/Sources/Apps/Toolbox/ResEd!2

# Copied .gitignore
* RiscOS/Sources/SystemRes/DesktopBoot!2
* RiscOS/Sources/Utilities/Patches/PatchesPatch!1

# Other changes to version controlled files
* RiscOS/Sources/FileSys/FSLock!2
* RiscOS/Sources/FileSys/PCCardFS/PCCardFS!2
* RiscOS/Sources/Internat/IntKey!1
* RiscOS/Sources/Video/Render/Colours!1
* RiscOS/Sources/Video/Render/Super!1

# No need to install C++Library
* RiscOS/Env!2
* RiscOS/BuildSys!10

# Relink if moved
* RiscOS/BuildSys!5
* RiscOS/Sources/Lib/RISC_OSLib!4

# Kernel Changes
* RiscOS/Sources/Kernel!17
* RiscOS/Sources/Kernel!34
* RiscOS/Sources/Kernel!35
* RiscOS/Sources/Kernel!36
* RiscOS/Sources/Kernel!37
* RiscOS/Sources/Kernel!38
* RiscOS/Sources/Kernel!40

# Various other changes
* RiscOS/Sources/Apps/Draw!2
* RiscOS/Sources/Apps/Paint!7
* RiscOS/Sources/Desktop/Wimp!21
* RiscOS/Sources/FileSys/FileSwitch!2
* RiscOS/Sources/FileSys/FileSwitch!4 
* RiscOS/Sources/FileSys/FileSwitch!5
* RiscOS/Sources/FileSys/FileSwitch!6
* RiscOS/Sources/Internat/Messages!1 
* RiscOS/Sources/Programmer/BASIC!2
* RiscOS/Sources/Programmer/BASIC!3
* RiscOS/Sources/Programmer/HdrSrc!6
* RiscOS/Sources/Programmer/HdrSrc!6 

# Cross compile support
* RiscOS/BuildSys!3
* RiscOS/Sources/Apps/SquashApp!1
* RiscOS/Sources/Desktop/Free!1
* RiscOS/Sources/Desktop/RedrawMgr!1
* RiscOS/Sources/FileSys/ADFS/ADFS!1
* RiscOS/Sources/FileSys/FileCore!1
* RiscOS/Sources/FileSys/FileSwitch!3
* RiscOS/Sources/FileSys/SCSIFS/SCSIFS!1
* RiscOS/Sources/HWSupport/Buffers!1
* RiscOS/Sources/HWSupport/CD/CDFSDriver!1
* RiscOS/Sources/HWSupport/DMA!1
* RiscOS/Sources/HWSupport/DeviceFS!1
* RiscOS/Sources/HWSupport/PCI!3
* RiscOS/Sources/HWSupport/Podule!1
* RiscOS/Sources/HWSupport/PortableHAL!1
* RiscOS/Sources/HWSupport/SCSI/SCSIDriver!1
* RiscOS/Sources/HWSupport/SD/SDIODriver!1
* RiscOS/Sources/HWSupport/Sound/Sound0HAL!1
* RiscOS/Sources/HWSupport/VFPSupport!1
* RiscOS/Sources/Internat/Territory/TerritoryManager!1
* RiscOS/Sources/Lib/AsmUtils!1
* RiscOS/Sources/Lib/RISC_OSLib!5
* RiscOS/Sources/Lib/TCPIPLibs!2
* RiscOS/Sources/Networking/Econet!1
* RiscOS/Sources/Networking/Fetchers/Gopher!1
* RiscOS/Sources/Printing/PDumpers!1
* RiscOS/Sources/Programmer/HostFS!2
* RiscOS/Sources/Programmer/Squash!1
* RiscOS/Sources/Toolbox/ToolboxLib!2
* RiscOS/Sources/Video/Render/BlendTable!1

# Memory mangement by Jeffery Lee
* RiscOS/Sources/Kernel!15
* RiscOS/Sources/HWSupport/ATA/SATADriver!1

# Various other (WIP) changes
* RiscOS/Sources/Programmer/Obey!1

# Linux port
* RiscOS/Sources/Programmer/HdrSrc!8


