#!/bin/bash

run() {
  mkdir -p /var/lib/dbus/ /run /b
  export PATH="/usr/lib/ccache:$PATH"
  dbus-uuidgen --ensure
  cd /b
  /s/configure CFLAGS="-g -O0 -fno-omit-frame-pointer -fsanitize=address -fsanitize=pointer-compare -fsanitize=pointer-subtract" --prefix /i --enable-video-wayland
  make install -j$(getconf _NPROCESSORS_ONLN) -k
  cd /
  gcc /test.c -O0 -g -fno-omit-frame-pointer -fsanitize=address -fsanitize=pointer-compare -fsanitize=pointer-subtract $(/i/bin/sdl2-config --cflags --libs)  || exit 125

  touch flag
  kwin &
  kwin=$!
  sleep 1
  (ASAN_OPTIONS=detect_invalid_pointer_pairs=2 ./a.out; rm flag) &
  testcase=$!
  sleep 1

  i=1
  while [[ -e flag && $i -lt 10000 ]]; do
    wmctrl -r :ACTIVE: -e 0,$(( $RANDOM / 32 )),$(( $RANDOM / 32 )),$(( $RANDOM / 32 )),$(( $RANDOM / 32 ))
    i=$(( $i + 1 ))
    sleep 0.01
    echo $i
  done

  rm flag
  kill $testcase
  kill $kwin
  echo $i resizes
  [[ $i -eq 10000 ]]
}

export -f run
mkdir -p ccache

bwrap --unsetenv TMPDIR --unshare-all                \
--ro-bind /bin /bin                                  \
--ro-bind /lib /lib                                  \
--ro-bind /usr /usr                                  \
--ro-bind-try /lib64 /lib64                          \
--ro-bind-try /etc/alternatives /etc/alternatives    \
--bind ccache /c --setenv CCACHE_DIR /c              \
--ro-bind . /s                                       \
--proc /proc                                         \
--dev /dev                                           \
--dir /tmp                                           \
--file 9 /test.c 9<<END                              \
bash xvfb-run bash -c run

#include <SDL.h>
#include <SDL_video.h>
#include <SDL_surface.h>

static char *pixels[1024*1024];

int main(int argc, char **argv) {

  SDL_Init(SDL_INIT_VIDEO);
  SDL_Window *window = SDL_CreateWindow("TEST", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
  SDL_Surface *screen = SDL_CreateRGBSurfaceFrom((void*)pixels, 640, 480, 8, 480, 0, 0, 0, 0);

  while(1) {
    SDL_Event e;
    SDL_WaitEvent(&e);
    switch(e.type) {
      case SDL_WINDOWEVENT:
        if (e.window.event == SDL_WINDOWEVENT_RESIZED) {
          for(int i = 1; i != 10; ++i) {
            SDL_Delay(1);
            SDL_PumpEvents();
          }
          SDL_SetWindowSize(window, e.window.data1, e.window.data2);
        }
        break;
      case SDL_QUIT:
        return 0;
    }
    SDL_BlitSurface(screen, 0, SDL_GetWindowSurface(window), 0);
    SDL_UpdateWindowSurface(window);
  }
}
END
