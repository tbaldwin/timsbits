// Test for seccomp SECCOMP_RET_TRAP incorrect return address and missing signals
// Compile with gcc -no-pie -fno-pie sigsys_test.c
// Use -marm on 32-bit ARM

// This file is public domain.

#define _GNU_SOURCE
#include <signal.h>
#include <stdio.h>
#include <sys/ucontext.h>
#include <sys/prctl.h>
#include <stdint.h>
#include <sys/resource.h>
#include <linux/bpf.h>
#include <linux/filter.h>
#include <linux/seccomp.h>
#include <errno.h>
#include <sys/syscall.h>

extern char after_sys[], before_sys[];

#ifdef __x86_64__
void handler(int i, siginfo_t *info, void *c) {
  ucontext_t *uc = c;
  if (uc->uc_mcontext.gregs[REG_RIP] == (uintptr_t)after_sys) return;

  printf("RAX:       %lli\nBefore:    %p\ncall_addr: %p\nRIP:       %p\nAfter:     %p\n\n",
         uc->uc_mcontext.gregs[REG_RAX],
         before_sys,
         info->si_call_addr,
         (void *)uc->uc_mcontext.gregs[REG_RIP],
         after_sys);
}

void __attribute__((noinline)) try(int64_t i) {
  asm volatile ("\nbefore_sys: syscall\nafter_sys:\n": "+a"(i) ::"memory");
}
#elif defined(__arm__)

void handler(int i, siginfo_t *info, void *c) {
  ucontext_t *uc = c;
  if (uc->uc_mcontext.arm_pc == (uintptr_t)after_sys) return;

  printf("R0:        %li\nBefore:    %p\ncall_addr: %p\nPC:        %p\nAfter:     %p\n\n",
         uc->uc_mcontext.arm_r0,
         before_sys,
         info->si_call_addr,
         (void *)uc->uc_mcontext.arm_pc,
         after_sys);
  uc->uc_mcontext.arm_pc = (uintptr_t)after_sys;
}

void __attribute__((noinline)) try(int i) {
  register int r0 asm("r0") = i;
  register int r7 asm("r7") = __NR_getpid;
  asm volatile ("\nbefore_sys: svc 0\nafter_sys:\n": "+r"(r0), "+r"(r7) ::"memory");
}
#elif defined(__aarch64__)

void handler(int i, siginfo_t *info, void *c) {
  ucontext_t *uc = c;
  if (uc->uc_mcontext.pc == (uintptr_t)after_sys) return;

  printf("X0:        %lli\nBefore:    %p\ncall_addr: %p\nPC:        %p\nAfter:     %p\n\n",
         uc->uc_mcontext.regs[0],
         before_sys,
         info->si_call_addr,
         (void *)uc->uc_mcontext.pc,
         after_sys);
  uc->uc_mcontext.pc = (uintptr_t)after_sys;
}

void __attribute__((noinline)) try(int i) {
  register int64_t x0 asm("x0") = i;
  register int64_t x8 asm("x8") = __NR_getpid;
  asm volatile ("\nbefore_sys: svc 0\nafter_sys:\n": "+r"(x0), "+r"(x8) ::"memory");
}
#else
#error CPU not supported.
#endif


struct sigaction x = {
  .sa_sigaction = handler,
  .sa_flags = SA_SIGINFO
};

int main() {
  sigaction(SIGSYS, &x, 0);

  struct sock_filter filter[] = {

    // Load instruction pointer
    {BPF_LD | BPF_W | BPF_ABS, 0, 0, 8},

    // Skip to SECCOMP_RET_ALLOW if not in test case.
    {BPF_JMP | BPF_JEQ | BPF_K, 1, 0, (uintptr_t)after_sys},
    {BPF_JMP | BPF_JEQ | BPF_K, 0, 1, (uintptr_t)before_sys},

    // Raise signal
    {BPF_RET | BPF_K, 0, 0, SECCOMP_RET_TRAP},

    // Continue as Linux system call
    {BPF_RET | BPF_K, 0, 0, SECCOMP_RET_ALLOW},
  };

  struct sock_fprog fprog = {
    .len = sizeof(filter) / sizeof(filter[0]),
    .filter = filter
  };

  prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0);
  prctl(PR_SET_SECCOMP, SECCOMP_MODE_FILTER, (uintptr_t)&fprog, 0, 0);

  puts("Starting");

  for(int i = 4000; i != -4100; --i)
    try(i);

  puts("Finished");
}
