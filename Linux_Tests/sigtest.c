#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <stdlib.h>

struct itimerspec x = {{0,100000000}, {0,100000000}};

volatile sig_atomic_t flag = 0;

#define message(X) write(1, (X), sizeof(X) - 1)

void signal_handler(int s) {
  sig_atomic_t x = flag;
  flag = 1;
  message(".");
  if (x) {
    message(" Recursion in signal handler!\n");
    abort();
  }
  flag = 0;
}

static struct sigaction sigact = {
  .sa_handler = &signal_handler,
  .sa_flags = SA_RESTART
};

int main (int argc, char **argv) {

  sigfillset(&sigact.sa_mask);
  sigaction(SIGRTMIN + 4, &sigact, 0);

  timer_t id;
  struct sigevent se = {
    .sigev_notify = SIGEV_SIGNAL,
    .sigev_signo = SIGRTMIN + 4,
    .sigev_value.sival_int = 500,
    //.sigev_notify_thread_id = gettid(),
  };
  timer_create(CLOCK_MONOTONIC, &se, &id);
  timer_settime(id, 0, &x, 0);

  char c;
  read(0, &c, 1);

  for (volatile int i = 400000000; i; --i) {}
  
  timer_delete(id);
  message("\n");
  return 0;

}
