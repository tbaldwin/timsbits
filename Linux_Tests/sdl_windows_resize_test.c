// Test with:
// gcc -Wall -g -O2 sdl_window_resize_test.c  `sdl2-config --cflags --libs`  ; ./a.out

#include <SDL.h>
#include <SDL_video.h>
#include <SDL_surface.h>

static char *pixels[1024*1024];

int main(int argc, char **argv) {
  
  //SDL_SetHint(SDL_HINT_VIDEO_ALLOW_SCREENSAVER, "1");
  SDL_Init(SDL_INIT_VIDEO);
  SDL_Window *window = SDL_CreateWindow("TEST", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
  SDL_Surface *screen = SDL_CreateRGBSurfaceFrom((void*)pixels, 640, 480, 8, 480, 0, 0, 0, 0);

  while(1) {
    SDL_Event e;
    SDL_WaitEvent(&e);
    switch(e.type) {
    case SDL_WINDOWEVENT:
        if (e.window.event == SDL_WINDOWEVENT_RESIZED) {
          for(int i = 1; i != 10; ++i) {
            SDL_Delay(1);
            SDL_PumpEvents();
          }
          SDL_SetWindowSize(window, e.window.data1, e.window.data2);
        }
        break;
      case SDL_QUIT:
        return 0;
    }
    SDL_BlitSurface(screen, 0, SDL_GetWindowSurface(window), 0);
    SDL_UpdateWindowSurface(window);
  }
}
