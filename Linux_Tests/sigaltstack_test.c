#define __GNU_SOURCE
#include <signal.h>
#include <stdio.h>
#include <error.h>
#include <errno.h>

char __attribute__((aligned)) stack[MINSIGSTKSZ];

int main(void) {
  stack_t ss = {
    .ss_sp = stack,
    .ss_flags =0,
    .ss_size = sizeof(stack)
  };

  int ret = sigaltstack(&ss, 0);

  if (ret) error(1, errno, "sigaltstack failed");

  puts("Success");
}
