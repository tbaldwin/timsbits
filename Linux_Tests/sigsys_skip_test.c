// Test for seccomp SECCOMP_RET_TRAP for missing signals
// Compile with gcc -no-pie -fno-pie sigsys_skip_test.c
// Use -marm on 32-bit ARM

// This file is public domain.

#define _GNU_SOURCE
#include <signal.h>
#include <stdio.h>
#include <sys/prctl.h>
#include <stdint.h>
#include <linux/bpf.h>
#include <linux/filter.h>
#include <linux/seccomp.h>
#include <stdbool.h>

extern char after_sys[];
volatile bool hit;

void handler(int i, siginfo_t *info, void *c) {
  hit = true;
}

struct sigaction x = {
  .sa_sigaction = handler,
  .sa_flags = SA_SIGINFO
};

int main() {
  sigaction(SIGSYS, &x, 0);

  struct sock_filter filter[] = {

    // Load instruction pointer
    {BPF_LD | BPF_W | BPF_ABS, 0, 0, 8},

    // Skip to SECCOMP_RET_ALLOW if not in test case.
    {BPF_JMP | BPF_JEQ | BPF_K, 0, 1, (uintptr_t)after_sys},

    // Raise signal
    {BPF_RET | BPF_K, 0, 0, SECCOMP_RET_TRAP},

    // Continue as Linux system call
    {BPF_RET | BPF_K, 0, 0, SECCOMP_RET_ALLOW},
  };

  struct sock_fprog fprog = {
    .len = sizeof(filter) / sizeof(filter[0]),
    .filter = filter
  };

  prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0);
  prctl(PR_SET_SECCOMP, SECCOMP_MODE_FILTER, (uintptr_t)&fprog, 0, 0);

  puts("Starting");

  for(int i = 4100; i != -4100; --i) {
    hit = false;
#ifdef __arm__
  register int r0 asm("r0") = 0;
  register int r7 asm("r7") = i;
  asm volatile ("\nsvc 0\nafter_sys:\n": "+r"(r0), "+r"(r7) ::"memory");
#elif defined(__aarch64__)
  register int64_t x0 asm("x0") = 0;
  register int64_t x8 asm("x8") = i;
  asm volatile ("\nsvc 0\nafter_sys:\n": "+r"(x0), "+r"(x8) ::"memory");
#else
#error CPU not supported.
#endif
    if (!hit) printf("SIGSYS missing for syscall %i\n", i);
  }

  puts("Finished");
}
