#include <sys/ptrace.h>
#include <asm/ptrace.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

int main(int argc, char **argv) {
  int pid = fork();
  if (pid == 0) {
    printf("%i\n", 0);
    ptrace(PTRACE_TRACEME);
    raise(SIGSTOP);
    for(int i = 1; i != 11; ++i)
      printf("%i\n", i);
  } else {
    for(int i = 1; i != 6; ++i) {
      wait(0);
      ptrace(PTRACE_SYSEMU, pid, 0, 0);
      wait(0);
      ptrace(PTRACE_SYSCALL, pid, 0, 0);
    }
    wait(0);
    ptrace(PTRACE_DETACH, pid, 0, 0);
  }
}
