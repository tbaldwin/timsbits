#define _GNU_SOURCE

#include <errno.h>
#include <linux/filter.h>
#include <linux/seccomp.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/prctl.h>
#include <unistd.h>

extern char syscall_instruction[];

int main(int argc, char **argv) {
    
    struct sock_filter filter[] = {

    // Load PC
    BPF_STMT(BPF_LD | BPF_W | BPF_ABS, offsetof(struct seccomp_data, instruction_pointer)),

    // Trap test instruction
    BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, (uintptr_t)syscall_instruction, 0, 1),

    // Ignore system call.
    BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_ERRNO | ENOSYS),

    // Continue as Linux system call.
    BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_ALLOW),
    };

    struct sock_fprog fprog = {
    .len = sizeof(filter) / sizeof(filter[0]),
    .filter = filter
    };

  prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0);
  prctl(PR_SET_SECCOMP, SECCOMP_MODE_FILTER, (uintptr_t)&fprog, 0, 0);
  
  int syscall = 4096;
  if (argc > 1) syscall = atoi(argv[1]);
  
  const char *c = "Hello World\n";

  while (*c) {
#ifdef __arm__
    register int r0 asm("r0") = *c;
    register int r7 asm("r7") = syscall;
    asm volatile ("svc 0\nsyscall_instruction:" : "+r"(r0), "+r"(r7) :: "memory");
#else
    int s = syscall;
    asm volatile ("syscall\nsyscall_instruction:" : "+a"(s):  "D"(*c) : "memory");
#endif
    write(2, c, 1);
    ++c;
  }
}
