#!/bin/bash

set -x
set -o pipefail
unset "${!GIT_@}"

git cherry-pick -n f7f279ceffe82e55e7ff19e9ded65c3be884e16e || exit 125
bwrap --ro-bind /btrfs/20191108-1350-root/ / --tmpfs /tmp --dev /dev --proc /proc --bind /home /home make -C ../qemu-build -j4 || exit 125

printf 'BASIC\nSYS "IXSupport_LinuxSyscall",,,,,,,,248\n' | timeout 10 bwrap --unshare-all --proc /proc --ro-bind /usr /usr --ro-bind /lib64 /lib64 --symlink /usr/lib /lib --ro-bind ~/RISC_OS/src/Unix/RISC_OS/RISC_OS /r --ro-bind ../qemu-build/arm-linux-user/qemu-arm /qemu --dev /dev /qemu /r  || exit 1
r=$?

git cherry-pick --abort
git reset --hard

exit $r
