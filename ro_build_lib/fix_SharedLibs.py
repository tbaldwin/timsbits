#!/usr/bin/python3

import struct
import os

bad = struct.pack("<IIIIIIIIIII",
  0x65675f5f, 0x70635f74, 0x72615f75,
  0x00006863, 0xff000010, 0xe92d4000,
  0xe10f1000, 0xef020016, 0x63e00000,
  0x7e100f10, 0xe121f001
)

good = struct.pack("<IIIIIIIIIII",
  0x65675f5f, 0x70635f74, 0x72615f75,
  0x00006863, 0xE3A00000, 0xE1A0F00E,
  0xe10f1000, 0xef020016, 0x63e00000,
  0x7e100f10, 0xe121f001
)

bad2 = struct.pack("<IIIIII",
  0xe13f000f, 0x133ff000, 0x010f0000,
  0x03c000cf, 0x0121f000, 0xe1a00000
)

good2 = struct.pack("<IIIIII",
  0xef02007c, 0x633ff000, 0xe1a00000,
  0xe1a00000, 0xe1a00000, 0xe1a00000,
)

assert len(bad) == len(good)

for root, dirs, files in os.walk("Apps/!SharedLibs"):
    for file in files:
        file = os.path.join(root, file)
        with open(file, "r+b") as f:
            data = f.read()
            data2 = data.replace(bad, good).replace(bad2, good2)
            if data != data2:
                print("Fixing ", file)
                f.seek(0)
                f.write(data2)
