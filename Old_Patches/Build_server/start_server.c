/*
  (define sresult)
  (define spair (socketpair PF_UNIX SOCK_SEQPACKET 0))
  (when (zero? (primitive-fork))
    (close-port (car spair))
    (set! sresult (select (vector (cdr spair)) #() (vector (cdr spair))))
    (when (> (vector-length (list-ref sresult 0)) 0)
      (dup2 (open-fdes "/dev/null" O_RDONLY) 0)
      (dup2 (open-fdes "Built/build_boot_log" (logior O_WRONLY O_CREAT)) 1)
      (dup2 1 2)
      (display "START")
      (system "ls -l /proc/self/fd")
      (execlp "echo" "echo" (number->string (port->fdes (cdr spair))))
    )
    (primitive-_exit 0)
  )
  (close-port (cdr spair))
  (port->fdes (car spair))
*/

#include <sys/types.h>
#include <sys/socket.h>

int plugin_is_GPL_compatible;

static char *start(const char *nm, int argc, char **argv) {
  int s[2];
  if (socketpair(AF_UNIX, SOCK_SEQPACKET, 0, s)) {
    perror("socketpair failed");
    return 0;
  }

  int pid = fork();
  if (pid < 0) {
    perror("fork failed");
    return 0;
  }

  if (pid == 0) {
    prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0);

    dup2(open("/dev/null", O_RDONLY | O_CLOEXEC), 0);
    dup2(open("Built/build_boot_log", O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC), 1);
    dup2(1, 2);

    char *args = calloc(argc + 1, sizeof(char *);
    memcpy(args, argv, argc * sizeof(char *));
    execvp(argv[0], args);
    _exit(1)
  }

  char *r = gmk_alloc(10)
  sprintf(r, "%i", pid);
  return r;
}

int start_server_gmk_setup(void) {
  gmk_add_function("start_server", start, 1, 0, 0);
  return 1;
}
