#!/usr/bin/python3

import pygit2
import sys

sys.setrecursionlimit(100000)

def write(c):
    parents = [write(p) for p in c.parents]
    tree = c.tree_id
    try:
        tree = repo.references["refs/replace/" + str(tree)].target
    except KeyError:
        pass
    return repo.create_commit(None, c.author, c.committer, c.message, tree, parents)

repo = pygit2.Repository(".")

for ref in repo.references:
    if ref.startswith("refs/heads/Built") and ref.endswith("-unified"):
        print("Updating", ref)
        repo.references.create(ref + "-Images", write(repo[repo.references[ref].target]), force=True)
