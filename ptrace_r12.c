#include <assert.h>
#include <stdio.h>
#include <sys/ptrace.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
  int child = fork();
  if (child == 0) {
    ptrace(PTRACE_TRACEME, 0, 0, 0);

    int register r7 asm("r7") = SYS_kill;
    int register r0 asm("r0") = getpid();
    int register r1 asm("r1") = SIGSTOP;

    int register r10 asm("r10") = 100;
    int register r12 asm("r12") = 120;
    asm volatile ("svc 0" : "+r"(r0), "+r"(r1), "+r"(r10), "+r"(r12) : "r"(r7));
    r7 = SYS_getpid;
    asm volatile ("svc 0" : "+r"(r10), "+r"(r12) : "r"(r7) : "r0");
    printf("After syscalls: r10 = %i r12 = %i\n", r10, r12);

  } else {
    int status;
    unsigned r[18];
    for(int i = 1; i <= 3; ++i) {
      waitpid(child, &status, __WALL);
      ptrace(PTRACE_GETREGS, child, 0, r);
      printf("Via ptrace:     r10 = %i r12 = %i\n", r[10], r[12]);
      r[10]++;
      r[12]++;
      ptrace(PTRACE_SETREGS, child, 0, r);
      ptrace(PTRACE_SYSCALL, child, 0, 0);
    }

    ptrace(PTRACE_DETACH, child, 0 ,0);
    waitpid(child, &status, __WALL);
  }
}
