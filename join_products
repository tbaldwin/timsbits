#!/usr/bin/python3

# Copyright 2020 Timothy Baldwin
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pygit2
import sys
import os
import heapq
import re

repo = pygit2.Repository(".")

def cid(c):
    return (2,) if c is None else (1, c.committer.time, c.message)

class Head:
    def __lt__(a, b):
        return cid(a.queue[-1]) < cid(b.queue[-1])

heads = []

for s in repo.references:
    if s.startswith("refs/heads/Products/") and not s.endswith("-unified"):
        d = [None]
        c = repo[repo.references[s].target]
        while True:
            d.append(c)
            if not c.parents:
                break
            c = c.parents[0]
        h = Head()
        h.queue = d
        h.name = s[20:]
        h.bname = h.name.encode()
        h.commit = None
        heads.append(h)

heads.sort(key=lambda a: a.name)


rebase_modules_re = re.compile(rb'^\[[ \t]*submodule[ \t]+"|^[ \t]+path *= *', re.MULTILINE)
c2 = []

while True:
    c = min(heads).queue[-1]
    if c is None:
        break

    for h in heads:
        if cid(h.queue[-1]) == cid(c):
            h.commit = h.queue.pop()

    tb = repo.TreeBuilder()
    gitmodules = b""
    for h in heads:
        if h.commit is not None:
            tb.insert(h.name, h.commit.tree_id, pygit2.GIT_FILEMODE_TREE)
            gitmodules += rebase_modules_re.sub(lambda m: m[0] + h.bname + b"/", h.commit.tree[".gitmodules"].data)
    tb.insert(".gitmodules", repo.create_blob(gitmodules), pygit2.GIT_FILEMODE_BLOB)
    c2 = [repo.create_commit(None, c.author, c.committer, c.message, tb.write(), c2)]

repo.references.create("refs/heads/master", c2[0], force=True)
